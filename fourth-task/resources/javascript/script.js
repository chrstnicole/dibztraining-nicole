$(function() {
    
    var CatCollection = [];

    function Cat(config) {
        this.img = config.img
    }

    for (var x=0; x<4; x++) {
        var imgName = 'cat-' + ((x % 4) + 1) + '.png';

        CatCollection.push(new Cat({
            img : 'resources/photos/' + imgName
        }));
    }

    var CatRenderer = function() {
        this.render = function() {

            var _html = [];
            var _self = this;
            CatCollection.forEach(function(data) {
                var view = _self.view(data);
                _html.push(view);
            });

            $('.main .row').html(_html.join(''));
        }

        this.view = function(Cat) {
            var _html = '<div class="cat">' + 
                            '<img src="' + Cat.img + '" alt="">' +
                            '<input class="num" value=0>' +
                            '<button class="btn btn-primary btn-lg btn-cat">' +
                            '<i class="fa fa-plus" aria-hidden="true"></i>' +
                            '</button>' +
                        '</div>';
            return _html;
        }
    }

    var renderer = new CatRenderer();
    renderer.render();

    $('.main').on('click', '.btn-cat', function() {
        var _this = $(this),
            _counter = _this.closest('.cat').find('.num'),
            _val = _counter.val();
        _counter.val(++_val);
    });
});


