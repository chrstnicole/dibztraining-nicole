# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is intended
  for training purposes only.

### Guidelines? ###

* 1. Forked this repository and start
  your development. Make sure you have GIT
  installed in your machine.

* 2. Add each task in trello Board

* 3. Don't forget to push your changes to 
  your forked repository before the day ends.

* 4. Make a pull request to the original Repository
  for code review.
  
  
* REFER TO THIS LINK FOR A CLEANER CODING: http://codeguide.co/

